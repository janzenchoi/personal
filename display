#!/bin/bash

# Constants
INTERSECTION="+"
THIN_SEPARATOR="-"
THICK_SEPARATOR="="
VERT_SEPARATOR="|"

# Repeats a string
function repeat_string() {
    string=$1
    repetitions=$2
    output=$(printf "%-${repetitions}s" "$string")
    echo "${output// /$string}"
}

# Returns a horizontal separator based on a maximum length of columns array
function horizontal_separator() {
    separator=$1 && shift
    max_lengths=("$@")
    echo -n $INTERSECTION
    for max_length in ${max_lengths[@]}; do
        if [[ $max_length == 0 ]]; then
            column_length=3
        else
            column_length=$(($max_length + 2))
        fi
        echo -n $(repeat_string "$separator" $column_length)$INTERSECTION
    done
    echo -e ""
}

# Adds padding ($2) to the left and right of a string ($1)
function apply_padding() {
    padding=$2
    left_padding=$(($2/2))
    right_padding=$(($padding-$left_padding))
    if [[ "$padding" > 0 ]]; then
        [[ "$left_padding" > 0 ]] && echo -n "$(repeat_string " " $left_padding)"
        echo -n " $1 "
        [[ "$right_padding" > 0 ]] && echo -n "$(repeat_string " " $right_padding)"
    else
        echo -n " $1 "
    fi
}

# Creates a table to display the data
function table() {

    # Parse inputs ($1=fields, $2=data)
    fields=$1
    row_array="$(echo -e "$2")"

    # Convert input to table contents
    if [[ "${row_array[@]}" == "" ]]; then
        row_array="-"
    fi
    row_array="$fields"$'\n'"$row_array"

    # Prepare data as 2D arraythick_separator
    max_lengths=()
    for (( i=1; i<=$num_columns; i++ )); do
        max_lengths+=(1);
    done

    # Determine maximum length of columns
    for row in ${row_array[@]}; do
        IFS="," read -a column_array <<< $row
        for (( i=0; i<=${#column_array[@]}-1; i++ )); do
            if [[ "${#column_array[$i]}" -gt "${max_lengths[$i]}" ]]; then
                max_lengths[$i]=${#column_array[$i]}
            fi
        done
    done

    # Print the table
    horizontal_separator $THICK_SEPARATOR "${max_lengths[@]}"
    for (( i=0; i<=${#row_array[@]}-1; i++ )); do
        row=${row_array[$i]}

        # Prepare to print row
        echo -n $VERT_SEPARATOR
        IFS="," read -a column_array <<< $row

        # Print data in row
        for (( j=0; j<=$num_columns-1; j++ )); do
            column=${column_array[$j]}

            # Placeholder '-' for empty cells
            if [[ "$column" == "" ]]; then
                column="-"
            fi


            # Apply padding
            padding=$((${max_lengths[$j]} - ${#column}))
            padded=$(apply_padding $column $padding)

            # Bolden first row and print
            if [ $i == 0 ]; then
                padded="\\033[1m$padded\\033[0m"
            fi
            echo -en $padded$VERT_SEPARATOR
        done
        echo ""

        # Print separator and start next row
        if [[ $i == 0 ]]; then
            horizontal_separator $THICK_SEPARATOR "${max_lengths[@]}"
        else
            horizontal_separator $THIN_SEPARATOR "${max_lengths[@]}"
        fi
    done
}
